## O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

- Code Review.
- Ant design beauty pageant.
- Replace the mock API with a local backend interface.
- Learn what cross domain is and how to solve it.
- Based on the front-end content we learned this week, draw a Concept Map to illustrate what we have learned from the front-end.
- Presentation.

## R (Reflective): Please use one word to express your feelings about today's class.

Compact.

## I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

- I am not very familiar with the theme of tonight's Concept Map and it is relatively difficult to draw.
- I am still a bit nervous during the presentation, and if I feel nervous, I will forget my words.

## D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

- Exercise my ability to give speeches on stage more.
- In development, it is necessary to be more careful and gradually troubleshoot any problems encountered.
