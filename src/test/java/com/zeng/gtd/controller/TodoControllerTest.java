package com.zeng.gtd.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zeng.gtd.dto.TodoRequest;
import com.zeng.gtd.entity.Todo;
import com.zeng.gtd.exception.TodoNotFoundException;
import com.zeng.gtd.repository.TodoRepository;
import com.zeng.gtd.service.TodoService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@AutoConfigureMockMvc
class TodoControllerTest {

    @Autowired
    private MockMvc client;

    @Autowired
    private TodoRepository todoRepository;

    @Autowired
    private TodoService todoService;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    void setUp() {
        todoRepository.deleteAll();
    }

    @Test
    void should_return_todos_when_perform_get_given_todos_in_db() throws Exception {
        // given
        Todo readBook = new Todo("read 100 books", false);
        Todo doSport = new Todo("do sport", false);
        todoRepository.saveAll(List.of(readBook, doSport));

        // when  then
        client.perform(MockMvcRequestBuilders.get("/todos"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].text").value(readBook.getText()))
                .andExpect(jsonPath("$[0].done").value(readBook.getDone()))
                .andExpect(jsonPath("$[1].text").value(doSport.getText()))
                .andExpect(jsonPath("$[1].done").value(doSport.getDone()));
    }

    @Test
    void should_return_todo_when_perform_get_by_id_given_todo_in_db() throws Exception {
        // given
        Todo readBook = new Todo("read 100 books", false);
        Todo save = todoRepository.save(readBook);

        // when  then
        client.perform(MockMvcRequestBuilders.get("/todos/" + save.getId()))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(readBook)));
    }

    @Test
    void should_insert_todo_when_perform_post_given_todo_not_in_db() throws Exception {
        // given
        TodoRequest dailyReport = new TodoRequest("write daily report", false);

        // when  then
        client.perform(MockMvcRequestBuilders.post("/todos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dailyReport)))
                .andExpect(jsonPath("$.text").value(dailyReport.getText()))
                .andExpect(jsonPath("$.done").value(dailyReport.getDone()));
    }

    @Test
    void should_update_todo_when_perform_put_given_todo_in_db() throws Exception {
        // given
        Todo dailyReport = new Todo("write daily report", false);
        Todo save = todoRepository.save(dailyReport);
        Todo updateTodo = new Todo();
        BeanUtils.copyProperties(save, updateTodo);
        updateTodo.setText("write report");
        updateTodo.setDone(true);

        // when  then
        client.perform(MockMvcRequestBuilders.put("/todos/" + save.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(updateTodo)))
                .andExpect(jsonPath("$.text").value(updateTodo.getText()))
                .andExpect(jsonPath("$.done").value(updateTodo.getDone()));
    }

    @Test
    void should_delete_todo_when_perform_delete_given_todo_id_in_db() throws Exception {
        // given
        Todo dailyReport = new Todo("write daily report", false);
        Todo save = todoRepository.save(dailyReport);


        // when  then
        client.perform(MockMvcRequestBuilders.delete("/todos/" + save.getId()))
                .andExpect(status().isOk());

        Assertions.assertNull(todoRepository.findById(save.getId()).orElse(null));
    }

    @Test
    void should_throw_exception_when_perform_get_by_id_given_todo_not_in_db() throws Exception {
        // given
        Todo dailyReport = new Todo("write daily report", false);
        Todo save = todoRepository.save(dailyReport);
        todoRepository.delete(save);

        // when  then
        Assertions.assertThrows(TodoNotFoundException.class, () -> todoService.getById(save.getId()));
    }
}