package com.zeng.gtd.service;

import com.zeng.gtd.dto.TodoRequest;
import com.zeng.gtd.dto.TodoResponse;
import com.zeng.gtd.entity.Todo;
import com.zeng.gtd.exception.TodoNotFoundException;
import com.zeng.gtd.repository.TodoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

import static com.zeng.gtd.mapper.TodoMapper.*;

@Service
@RequiredArgsConstructor
public class TodoService {

    private final TodoRepository todoRepository;

    public List<TodoResponse> list() {
        return getTodoResponseList(todoRepository.findAll());
    }

    public TodoResponse getById(Integer id) {
        return getTodoResponse(todoRepository.findById(id).orElseThrow(TodoNotFoundException::new));
    }

    public TodoResponse createTodo(TodoRequest todoRequest) {
        Todo todo = getTodoEntity(todoRequest);
        Todo save = todoRepository.save(todo);
        return getTodoResponse(save);
    }

    public TodoResponse updateTodo(Integer id, TodoRequest todoRequest) {
        Todo todo = todoRepository.findById(id).orElseThrow(TodoNotFoundException::new);
        if (todoRequest.getText() != null && !todoRequest.getText().equals("")){
            todo.setText(todoRequest.getText());
        }
        if (Objects.nonNull(todoRequest.getDone())){
            todo.setDone(todoRequest.getDone());
        }
        Todo save = todoRepository.save(todo);
        return getTodoResponse(save);
    }

    public void deleteById(Integer id) {
        todoRepository.deleteById(id);
    }
}
