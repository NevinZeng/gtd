package com.zeng.gtd.controller;

import com.zeng.gtd.dto.TodoRequest;
import com.zeng.gtd.dto.TodoResponse;
import com.zeng.gtd.service.TodoService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todos")
@RequiredArgsConstructor
public class TodoController {

    private final TodoService todoService;

    @GetMapping
    public List<TodoResponse> list(){
        return todoService.list();
    }

    @GetMapping("/{id}")
    public TodoResponse getById(@PathVariable Integer id){
        return todoService.getById(id);
    }

    @PostMapping
    public TodoResponse createTodo(@RequestBody TodoRequest todoRequest){
        return todoService.createTodo(todoRequest);
    }

    @PutMapping("/{id}")
    public TodoResponse updateTodo(@PathVariable Integer id, @RequestBody TodoRequest todoRequest){
        return todoService.updateTodo(id, todoRequest);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Integer id){
        todoService.deleteById(id);
    }
}
