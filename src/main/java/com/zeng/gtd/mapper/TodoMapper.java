package com.zeng.gtd.mapper;

import com.zeng.gtd.dto.TodoRequest;
import com.zeng.gtd.dto.TodoResponse;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

public class TodoMapper {

    public static List<TodoResponse> getTodoResponseList(List<com.zeng.gtd.entity.Todo> todoList){
        return todoList.stream().map(TodoMapper::getTodoResponse).collect(Collectors.toList());
    }

    public static TodoResponse getTodoResponse(com.zeng.gtd.entity.Todo todo) {
        TodoResponse todoResponse = new TodoResponse();
        BeanUtils.copyProperties(todo, todoResponse);
        return todoResponse;
    }

    public static com.zeng.gtd.entity.Todo getTodoEntity(TodoRequest todoRequest) {
        com.zeng.gtd.entity.Todo todo = new com.zeng.gtd.entity.Todo();
        BeanUtils.copyProperties(todoRequest, todo);
        return todo;
    }
}
